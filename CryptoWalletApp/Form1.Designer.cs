﻿
namespace CryptoWalletApp {
    partial class Form1 {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblState = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.tbState = new System.Windows.Forms.TextBox();
            this.btnAddCurrency = new System.Windows.Forms.Button();
            this.tbYourWallet = new System.Windows.Forms.TextBox();
            this.lblYourWallet = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(183, 39);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(150, 27);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Crypto Wallet";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(30, 90);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(191, 27);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Unesite naziv željene valute:";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblState
            // 
            this.lblState.Location = new System.Drawing.Point(296, 90);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(189, 27);
            this.lblState.TabIndex = 2;
            this.lblState.Text = "Unesite stanje željene valute:";
            this.lblState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(30, 134);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(191, 23);
            this.tbName.TabIndex = 3;
            // 
            // tbState
            // 
            this.tbState.Location = new System.Drawing.Point(296, 134);
            this.tbState.Multiline = true;
            this.tbState.Name = "tbState";
            this.tbState.Size = new System.Drawing.Size(189, 23);
            this.tbState.TabIndex = 4;
            // 
            // btnAddCurrency
            // 
            this.btnAddCurrency.Location = new System.Drawing.Point(198, 176);
            this.btnAddCurrency.Name = "btnAddCurrency";
            this.btnAddCurrency.Size = new System.Drawing.Size(113, 25);
            this.btnAddCurrency.TabIndex = 5;
            this.btnAddCurrency.Text = "Dodaj valutu";
            this.btnAddCurrency.UseVisualStyleBackColor = true;
            // 
            // tbYourWallet
            // 
            this.tbYourWallet.Location = new System.Drawing.Point(109, 281);
            this.tbYourWallet.Multiline = true;
            this.tbYourWallet.Name = "tbYourWallet";
            this.tbYourWallet.Size = new System.Drawing.Size(293, 241);
            this.tbYourWallet.TabIndex = 6;
            // 
            // lblYourWallet
            // 
            this.lblYourWallet.Location = new System.Drawing.Point(198, 238);
            this.lblYourWallet.Name = "lblYourWallet";
            this.lblYourWallet.Size = new System.Drawing.Size(113, 31);
            this.lblYourWallet.TabIndex = 7;
            this.lblYourWallet.Text = "Tvoj novčanik:";
            this.lblYourWallet.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(528, 562);
            this.Controls.Add(this.lblYourWallet);
            this.Controls.Add(this.tbYourWallet);
            this.Controls.Add(this.btnAddCurrency);
            this.Controls.Add(this.tbState);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.lblState);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblTitle);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.TextBox tbState;
        private System.Windows.Forms.Button btnAddCurrency;
        private System.Windows.Forms.TextBox tbYourWallet;
        private System.Windows.Forms.Label lblYourWallet;
    }
}

