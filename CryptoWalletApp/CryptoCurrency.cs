﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoWalletApp {
    class CryptoCurrency {
        public CryptoCurrency(string name, double state) {
            Name = name;
            State = state;
        }

        public string Name { get; set; }
        public double State { get; set; }
    }
}
